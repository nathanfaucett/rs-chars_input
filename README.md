chars_input
=====

chars input

```rust
extern crate chars_input;


use chars_input::{Chars, Input, State};


fn line_count(input: &mut Input, state: &mut State) -> usize {
    let mut lines = 0;

    while let Some(_) = input.read_line(state) {
        lines += 1;
    }

    lines
}


fn main() {
    let mut lines = Chars::new("Hello, world!\n some chars input.\n".chars());
    assert_eq!(line_count(&mut lines, &mut State::new()), 2);

    let mut state = State::new();
    let mut chars = "abcdef".chars().collect::<Vec<char>>();

    assert_eq!(chars.peek(&state, 0), Some('a'));
    assert_eq!(chars.peek(&state, 1), Some('b'));
    assert_eq!(chars.peek(&state, 2), Some('c'));

    chars.read_offset(&mut state, 3);

    assert_eq!(chars.read(&mut state), Some('d'));
    assert_eq!(chars.read(&mut state), Some('e'));
    assert_eq!(chars.read(&mut state), Some('f'));

    assert_eq!(state.index(), 6);
    assert_eq!(state.row(), 1);
    assert_eq!(state.col(), 6);

    assert_eq!(chars.is_done(&state), true);
}
```
