#![feature(alloc)]
#![cfg_attr(feature = "std", feature(io))]
#![no_std]

#[cfg(feature = "std")]
extern crate std;

extern crate alloc;

mod chars;
mod input;
mod lines;
mod state;

pub use self::chars::Chars;
pub use self::input::Input;
pub use self::lines::Lines;
pub use self::state::State;
